import { Component } from "@angular/core";
import { Types } from "src/app/types.constant";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  types = Types;

  constructor() {}
}
