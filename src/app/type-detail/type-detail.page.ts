import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Types } from "src/app/types.constant";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-type-detail",
  templateUrl: "./type-detail.page.html",
  styleUrls: ["./type-detail.page.scss"]
})
export class TypeDetailPage implements OnInit {
  selectedType = {};
  weaknessTypes: string[] = [];
  types = Types;

  constructor(private route: ActivatedRoute, private nav: NavController) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log("params", params);
      this.selectedType = this.types.find(type => type["name"] == params.type);
      console.log("this.selectedType", this.selectedType);
      this.weaknessTypes = this.selectedType["weakness"];
    });
  }

  back() {
    this.nav.back();
  }
}
