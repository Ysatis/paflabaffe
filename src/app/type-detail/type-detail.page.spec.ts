import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TypeDetailPage } from './type-detail.page';

describe('TypeDetailPage', () => {
  let component: TypeDetailPage;
  let fixture: ComponentFixture<TypeDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TypeDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
