export const Types: Array<Object> = [
  {
    name: "Eau",
    weakness: ["Plante", "Electrique"],
    color: "#4592C4"
  },
  {
    name: "Plante",
    weakness: ["Feu", "Glace", "Insecte", "Poiso", "Vol"],
    color: "#9BCC50"
  },
  {
    name: "Feu",
    weakness: ["Eau", "Roche", "Sol"],
    color: "#FD7D24"
  },
  {
    name: "Electrique",
    weakness: ["Sol"],
    color: "#EED535"
  },
  {
    name: "Normal",
    weakness: ["Combat"],
    color: "#A4ACAF"
  },
  {
    name: "Fée",
    weakness: ["Poison", "Acier"],
    color: "#FDB9E9"
  },
  {
    name: "Acier",
    weakness: ["Combat", "Feu", "Sol"],
    color: "#9EB7B8"
  },
  {
    name: "Psy",
    weakness: ["Insecte", "Spectre", "Ténèbre"],
    color: "#F366B9"
  },
  {
    name: "Spectre",
    weakness: ["Spectre", "Ténèbre"],
    color: "#B97FC9"
  },
  {
    name: "Ténèbre",
    weakness: ["Combat", "Fée", "Insecte"],
    color: "#707070"
  },
  {
    name: "Combat",
    weakness: ["Fée", "Psy", "Vol"],
    color: "#D56723"
  },
  {
    name: "Poison",
    weakness: ["Psy", "Sol"],
    color: "#B97FDA"
  },
  {
    name: "Dragon",
    weakness: ["Dragin", "Fée", "Glace"],
    color: "#F16E57"
  },
  {
    name: "Insecte",
    weakness: ["Feu", "Roche", "Vol"],
    color: "#729F3F"
  },
  {
    name: "Roche",
    weakness: ["Acier", "Combat", "Eau", "Plante", "Sol"],
    color: "A38C21"
  },
  {
    name: "Sol",
    weakness: ["Eau", "Glace", "Plante"],
    color: "#F7DE3F"
  },
  {
    name: "Vol",
    weakness: ["Electrique", "Glace", "Roche"],
    color: "#BDB9B8"
  },
  {
    name: "Glace",
    weakness: ["Acier", "Combat", "Feu", "Roche"],
    color: "#51C4E7"
  }
];
